package io.ysdisk.dao;

import io.ysdisk.domain.entity.UserStorageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * 继承JPA，进行简单的名称匹配DAO
 */
public interface UserStorageRepository extends JpaRepository<UserStorageEntity, Long> {

	Optional<UserStorageEntity> findByUserId(Long userId);

	List<UserStorageEntity> findAllByUserIdIn(Collection<Long> userIds);
}
