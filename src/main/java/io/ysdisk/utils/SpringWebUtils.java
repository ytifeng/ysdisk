package io.ysdisk.utils;

import io.ysdisk.domain.vo.LoginUser;
import io.ysdisk.exception.BizException;
import io.ysdisk.exception.msg.BizMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
/**
 * 从springmvc中的相关bean获取包括但不限于用户ip，response和request对象和用户信息
 */

public class SpringWebUtils {

	/**
	 * 获取用户真实IP
	 * 在用户登陆成功时会采集ip信息
	 * @return
	 */
	public static String getRemoteAddr() {
		HttpServletRequest request = getRequest();
		String ip = request.getHeader("x-forwarded-for");
		if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
			// 多次反向代理后会有多个ip值，第一个ip才是真实ip
			if (ip.indexOf(",") != -1) {
				ip = ip.split(",")[0];
			}
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Real-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 从SpringSecurity上下文中获取请求用户
	 *
	 * @return
	 */
	public static LoginUser getRequestUser() {
		Object principal = Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
				.map(Authentication::getPrincipal).orElse(null);
		if (principal instanceof LoginUser) {
			return (LoginUser) principal;
		}
		return null;
	}

	/**
	 * 获取用户登录信息，如用户未登录，抛出用户未登录异常
	 *
	 * @return
	 */
	public static LoginUser requireLogin() {
		return Optional.ofNullable(getRequestUser()).orElseThrow(() -> new BizException(BizMessage.USER_NOT_LOGIN));
	}


	/**
	 * 获取response对象
	 * @return
	 */
	public static HttpServletResponse getResponse(){
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		assert requestAttributes != null;
		return requestAttributes.getResponse();
	}

	/**
	 * 获取request对象
	 * @return
	 */
	public static HttpServletRequest getRequest(){
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		assert requestAttributes != null;
		return requestAttributes.getRequest();
	}
}
