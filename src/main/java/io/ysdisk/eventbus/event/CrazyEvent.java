package io.ysdisk.eventbus.event;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;


@Data
public abstract class CrazyEvent implements Serializable {

	/**
	 * 事件代码
	 */
	private String eventCode = UUID.randomUUID().toString();

	/**
	 * 创建时间
	 */
	private Date createTime = new Date();

	/**
	 * 延迟时间
	 */
	private long delay = 0L;

	//计算事件截止时间的方法
	public Date getDueDate(){
		long ms = createTime.getTime() + delay;
		return new Date(ms);
	}
}