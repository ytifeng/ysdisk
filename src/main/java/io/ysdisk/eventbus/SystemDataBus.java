package io.ysdisk.eventbus;

import com.google.common.eventbus.EventBus;
import io.ysdisk.eventbus.event.CrazyEvent;
import io.ysdisk.eventbus.listener.SyncLockListener;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

/**
 * 通过本类提供的方法，向EventBus发布任务
 */

@Slf4j
@Component
public class SystemDataBus implements InitializingBean {

	/**
	 * 计划线程池
	 * ThreadPoolTaskScheduler是Spring框架提供的一个基于线程池的任务调度器，用于在应用程序中调度异步任务和定时任务。
	 * 它是TaskScheduler接口的实现类，提供了灵活的任务调度功能，可以用于替代标准的Timer对象。
	 * 可以执行异步任务，不会阻塞主线程，提高应用程序的性能。
	 */
	@Autowired
	//注入自定义的线程池
	@Qualifier("delayExecutor")
	private ThreadPoolTaskScheduler delayThreadPoolExecutor;

	/**
	 * 监听者（消费者）
	 * 通过监听者控制加锁与解锁
	 *
	 */
	@Autowired
	private SyncLockListener syncLockListener;

	/**
	 * 同步事件总线
	 */
	/**
	 * 组件可以发布事件到EventBus，其他组件可以订阅这些事件并在事件发生时做出相应的响应。
	 * EventBus会负责将事件分发给所有订阅了该事件的组件，从而实现了组件间的通信。
	 */
	private final EventBus syncEventBus;

	/**
	 * 异步事件总线
	 */
	private final EventBus asyncEventBus;

	/**
	 * 初始化事件总线
	 * @param asyncEventBus
	 * @param syncEventBus
	 */
	public SystemDataBus(@Qualifier("asyncEventBus") EventBus asyncEventBus, @Qualifier("syncEventBus") EventBus syncEventBus) {
		this.asyncEventBus = asyncEventBus;
		this.syncEventBus = syncEventBus;
	}

	/**
	 * 同步
	 *发送事件
	 * @param event
	 */
	public void postSync(CrazyEvent event) {
		syncEventBus.post(event);
	}

	/**
	 * 异步
	 * 根据时间延迟发布
	 * @param event
	 */
	public void postAsync(CrazyEvent event) {
		log.info("Event raise with type " + event.getClass().getSimpleName() + " and delay " + event.getDelay());
		if (event.getDelay() <= 0L) {
			//为同步事件
			asyncEventBus.post(event);
		} else {
			//使用延迟线程池调度任务，本质上是延迟一定时间再发布同步事件（==异步事件）时间到会被订阅者处理，删除键值对
			delayThreadPoolExecutor.schedule(() ->
							Try.run(() -> asyncEventBus.post(event)).onFailure(e -> log.debug("Event Bus "+e.getMessage())), event.getDueDate());
		}
	}

	/**
	 * 注册Listener
	 * 将syncLockListener订阅者注册到两个总线中，开始订阅总线中的事件
	 */
	@Override
	public void afterPropertiesSet(){
		syncEventBus.register(syncLockListener);
		asyncEventBus.register(syncLockListener);
	}
}
