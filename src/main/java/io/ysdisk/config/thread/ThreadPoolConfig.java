package io.ysdisk.config.thread;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * 配置ThreadPoolTaskScheduler
 */
@Configuration
public class ThreadPoolConfig {

	/**
	 * 延时执行线程池
	 *
	 * @return
	 */
	@Bean(name = "delayExecutor")
	public ThreadPoolTaskScheduler scheduleThreadPoolExecutor() {
		//ThreadPoolTaskScheduler是Spring框架提供的一个线程池任务调度器，用于执行定时任务或者周期性任务
		ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
		//使用Runtime.getRuntime().availableProcessors()获取当前系统可用的处理器数量，并设置这个线程池的大小（setPoolSize）
		threadPoolTaskScheduler.setPoolSize(Runtime.getRuntime().availableProcessors());
		//调用setThreadNamePrefix("delay-thread-")为线程池中的线程设置一个前缀，这样可以在调试时更容易区分这些线程
		threadPoolTaskScheduler.setThreadNamePrefix("delay-thread-");
		//调用initialize()方法初始化线程池，然后返回这个已经配置好的线程池对象
		threadPoolTaskScheduler.initialize();
		return threadPoolTaskScheduler;
	}
}
