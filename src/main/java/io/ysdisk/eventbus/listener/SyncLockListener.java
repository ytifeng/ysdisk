package io.ysdisk.eventbus.listener;

import com.google.common.eventbus.Subscribe;
import io.ysdisk.eventbus.event.SyncLockEvent;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static io.ysdisk.eventbus.event.SyncLockEvent.LOCK_MAP;


@Slf4j
@Data
@Service
public class SyncLockListener {

	/**
	 * 注册方法
	 * 锁对象map的添加和移除
	 * @param event
	 */
	@Subscribe
	public void execute(SyncLockEvent event){
		if (!event.getUnlock()){
			if (!LOCK_MAP.containsKey(event.getKey())){
				LOCK_MAP.put(event.getKey(), event);
				log.info("加锁: [{}], [{}]", event.getKey(), LOCK_MAP);
			}
		}else{
			if (LOCK_MAP.containsKey(event.getKey())){
				Object remove = LOCK_MAP.remove(event.getKey());
				log.info("解锁: [{}], [{}]", event.getKey(), remove);
			}
		}
	}
}
