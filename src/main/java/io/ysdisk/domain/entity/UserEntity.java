package io.ysdisk.domain.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户
 *
 * @Author: Richard.Lee
 * @Date: created by 2021/2/20
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = {"username", "email"}))
public class UserEntity implements Serializable {

	/**
	 * 用户id
	 */
	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@GenericGenerator(name = "native", strategy = "native")
	private Long id;

	/**
	 * 用户名
	 */
	@Column(name = "username", nullable = false)
	private String username;

	/**
	 * 用户邮箱
	 */
	@Column(name = "email", nullable = false)
	private String email;

	/**
	 * 用户头像链接，指向UserFile的ID
	 */
	@Column(name = "image_file_id", length = 32)
	private String imgUrl;

	/**
	 * 加密后的密码
	 */
	@Column(name = "password", nullable = false, length = 64)
	private String password;


	/**
	 * 对应的角色
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id", nullable = false, foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
	private RoleEntity role;

	/**
	 * 注册时间
	 */
	@Column(name = "user_register_time", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerTime;

	/**
	 * 用户是否被锁定, 异常操作会被锁定
	 */
	@Column(name = "is_account_non_locked", nullable = false)
	private Boolean accountNonLocked;

	public UserEntity() {
	}

	public Long getId() {
		return this.id;
	}

	public String getUsername() {
		return this.username;
	}

	public String getEmail() {
		return this.email;
	}

	public String getImgUrl() {
		return this.imgUrl;
	}

	public String getPassword() {
		return this.password;
	}

	public RoleEntity getRole() {
		return this.role;
	}

	public Date getRegisterTime() {
		return this.registerTime;
	}

	public Boolean getAccountNonLocked() {
		return this.accountNonLocked;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public void setAccountNonLocked(Boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public boolean equals(final Object o) {
		if (o == this) return true;
		if (!(o instanceof UserEntity)) return false;
		final UserEntity other = (UserEntity) o;
		if (!other.canEqual((Object) this)) return false;
		final Object this$id = this.getId();
		final Object other$id = other.getId();
		if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
		final Object this$username = this.getUsername();
		final Object other$username = other.getUsername();
		if (this$username == null ? other$username != null : !this$username.equals(other$username)) return false;
		final Object this$email = this.getEmail();
		final Object other$email = other.getEmail();
		if (this$email == null ? other$email != null : !this$email.equals(other$email)) return false;
		final Object this$imgUrl = this.getImgUrl();
		final Object other$imgUrl = other.getImgUrl();
		if (this$imgUrl == null ? other$imgUrl != null : !this$imgUrl.equals(other$imgUrl)) return false;
		final Object this$password = this.getPassword();
		final Object other$password = other.getPassword();
		if (this$password == null ? other$password != null : !this$password.equals(other$password)) return false;
		final Object this$role = this.getRole();
		final Object other$role = other.getRole();
		if (this$role == null ? other$role != null : !this$role.equals(other$role)) return false;
		final Object this$registerTime = this.getRegisterTime();
		final Object other$registerTime = other.getRegisterTime();
		if (this$registerTime == null ? other$registerTime != null : !this$registerTime.equals(other$registerTime))
			return false;
		final Object this$accountNonLocked = this.getAccountNonLocked();
		final Object other$accountNonLocked = other.getAccountNonLocked();
		if (this$accountNonLocked == null ? other$accountNonLocked != null : !this$accountNonLocked.equals(other$accountNonLocked))
			return false;
		return true;
	}

	protected boolean canEqual(final Object other) {
		return other instanceof UserEntity;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $id = this.getId();
		result = result * PRIME + ($id == null ? 43 : $id.hashCode());
		final Object $username = this.getUsername();
		result = result * PRIME + ($username == null ? 43 : $username.hashCode());
		final Object $email = this.getEmail();
		result = result * PRIME + ($email == null ? 43 : $email.hashCode());
		final Object $imgUrl = this.getImgUrl();
		result = result * PRIME + ($imgUrl == null ? 43 : $imgUrl.hashCode());
		final Object $password = this.getPassword();
		result = result * PRIME + ($password == null ? 43 : $password.hashCode());
		final Object $role = this.getRole();
		result = result * PRIME + ($role == null ? 43 : $role.hashCode());
		final Object $registerTime = this.getRegisterTime();
		result = result * PRIME + ($registerTime == null ? 43 : $registerTime.hashCode());
		final Object $accountNonLocked = this.getAccountNonLocked();
		result = result * PRIME + ($accountNonLocked == null ? 43 : $accountNonLocked.hashCode());
		return result;
	}

	public String toString() {
		return "UserEntity(id=" + this.getId() + ", username=" + this.getUsername() + ", email=" + this.getEmail() + ", imgUrl=" + this.getImgUrl() + ", password=" + this.getPassword() + ", role=" + this.getRole() + ", registerTime=" + this.getRegisterTime() + ", accountNonLocked=" + this.getAccountNonLocked() + ")";
	}
}
