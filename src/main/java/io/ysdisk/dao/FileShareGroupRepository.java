package io.ysdisk.dao;

import io.ysdisk.domain.entity.FileShareGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileShareGroupRepository extends JpaRepository<FileShareGroupEntity, String> {


}
