package io.ysdisk.dao;

import io.ysdisk.domain.entity.ThumbnailEntity;
import io.ysdisk.domain.enums.ThumbnailTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ThumbnailRepository extends JpaRepository<ThumbnailEntity, Long> {

	@Query("from ThumbnailEntity where fileId=:fileId and type = :type")
	Optional<ThumbnailEntity> findByFileIdAndImageSize(@Param("fileId")String fileId, @Param("type")ThumbnailTypeEnum type);

	@Query("from ThumbnailEntity t where t.fileId not in (select f.id from FileEntity f)")
	List<ThumbnailEntity> findAllGarbage();
}
