package io.ysdisk.dao;

import io.ysdisk.domain.entity.FileShareEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface FileShareRepository extends JpaRepository<FileShareEntity, String> {

	List<FileShareEntity> findAllByPidIn(Collection<String> pids);

	List<FileShareEntity> findAllByFileShareGroupIdIn(Collection<String> shareGroupIds);
}
