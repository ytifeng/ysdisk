package io.ysdisk.service.impl;

import io.ysdisk.dao.RoleRepository;
import io.ysdisk.domain.entity.RoleEntity;
import io.ysdisk.domain.enums.RoleEnum;
import io.ysdisk.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class RoleSeviceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepository;


	/**
	 * 通过RoleEnum查询Role，如不存在该Role则创建
	 *
	 * @param roleEnum
	 * @return
	 */
	@Override
	public RoleEntity findByEnum(RoleEnum roleEnum) {
		return roleRepository.findById(roleEnum.getId()).orElseGet(()->{
			RoleEntity roleEntity = new RoleEntity();
			roleEntity.setId(roleEnum.getId());
			roleEntity.setName(roleEnum.name());
			return roleRepository.save(roleEntity);
		});
	}
}
