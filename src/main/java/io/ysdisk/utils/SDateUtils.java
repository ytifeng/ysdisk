package io.ysdisk.utils;

import java.util.Date;
import java.util.Objects;

/**
 *日期工具类，提供日期比较的功能，用于标定分享的文件或者文件组是否过期
 */
public class SDateUtils {

	/**
	 * 判断现在是否在thatDate之前, 当thatDate为null时，代表无穷大
	 * @param thatDate
	 * @return
	 */
	public static Boolean nowBefore(Date thatDate){
		Date now = new Date();
		if (Objects.isNull(thatDate)){
			return true;
		}
		return now.before(thatDate);
	}

	/**
	 * 判断现在是否在thatDate之后, 当thatDate为null时，代表无穷大
	 * @param thatDate
	 * @return
	 */
	public static Boolean nowAfter(Date thatDate){
		Date now = new Date();
		if (Objects.isNull(thatDate)){
			return false;
		}
		return now.before(thatDate);
	}
}
