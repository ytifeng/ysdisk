package io.ysdisk.controller;

import io.ysdisk.domain.dto.FileDTO;
import io.ysdisk.domain.dto.FileUploadDTO;
import io.ysdisk.domain.dto.MergeFileDTO;
import io.ysdisk.domain.entity.FileEntity;
import io.ysdisk.domain.vo.LoginUser;
import io.ysdisk.domain.vo.UploadFileVO;
import io.ysdisk.service.FileService;
import io.ysdisk.service.FileShareService;
import io.ysdisk.utils.FileUtils;
import io.ysdisk.utils.SpringWebUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import jodd.bean.BeanCopy;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.Objects;

@Slf4j
@Tag(name = "FileTransfer", description = "该接口为文件传输接口，主要用来做文件的上传和下载")
@RequestMapping("transfer")
@Validated
@RestController
public class FileTransferController {

    @Autowired
    private FileService fileService;

    @Autowired
    private FileShareService fileShareService;

    @Operation(summary = "极速上传", description = "校验文件MD5判断文件是否存在，如果存在直接上传成功并返回skipUpload=true，如果不存在返回skipUpload=false")
    @GetMapping("upload")
    @ResponseBody
    public ResponseEntity<UploadFileVO> uploadFileSpeed(@Validated FileUploadDTO fileUploadDto){
        LoginUser user = SpringWebUtils.requireLogin();
        //查询文件是否使用，并进行相应的修改
        FileEntity fileEntity = fileService.speedUpload(user.getId(), fileUploadDto);
        //查询该文件已上传切片序号
        Collection<Integer> uploaded = fileService.getUploaded(fileUploadDto.getIdentifier(), fileUploadDto.getChunkSize());
        return ResponseEntity.ok(new UploadFileVO(false, Objects.nonNull(fileEntity), uploaded));
    }


    @Operation(summary = "上传文件", description = "真正的上传文件接口")
    @PostMapping("upload")
    public ResponseEntity<UploadFileVO> uploadChunk(@Validated FileUploadDTO fileUploadDto) {
        LoginUser user = SpringWebUtils.requireLogin();
        //文件上传
        Collection<Integer> uploaded = fileService.upload(user.getId(), fileUploadDto);
        //利用uploaded.size()==fileUploadDto.getTotalChunks()判断是否需要合并
        return ResponseEntity.ok(new UploadFileVO(uploaded.size()==fileUploadDto.getTotalChunks(), false, uploaded));
    }

    @Operation(summary = "下载文件", description = "文件下载接口，保证文件安全，阻止非法用户下载")
    @GetMapping("download/{userFileId}")
    @Parameters({
            @Parameter(name = "userFileId", description = "用户文件id，需要登录用户的文件", required = true)
    })
    public void downloadFile(@PathVariable @NotBlank @Length(min = 32, max = 32) String userFileId){

        LoginUser user = SpringWebUtils.requireLogin();
        FileDTO fileDTO = fileService.getFileResource(user.getId(), user.getRole(), userFileId);
        FileUtils.sendFile(fileDTO);
    }

    @Operation(summary = "下载文件", description = "文件下载接口，保证文件安全，阻止非法用户下载")
    @GetMapping("/anonymous/download/{shareId}")
    @Parameters({
            @Parameter(name = "userFileId", description = "用户文件id，需要登录用户的文件", required = true)
    })
    public void anonymousDownloadFile(@PathVariable @NotBlank @Length(min = 32, max = 32) String shareId){

        FileDTO fileDTO = fileShareService.getFileResource(shareId);
        FileUtils.sendFile(fileDTO);
    }


    @Operation(summary = "获取略缩图", description = "略缩图获取，用于图片类型文件的略缩图和头像")
    @GetMapping("thumbnail/{userFileId}")
    @Parameters({
            @Parameter(name = "userFileId", description = "用户文件id，需要登录用户的文件", required = true)
    })
    public void thumbnail(@PathVariable @NotBlank @Length(min = 32, max = 32) String userFileId){

        LoginUser user = SpringWebUtils.requireLogin();
        FileDTO fileDTO = fileService.getThumbnail(user.getId(), userFileId);
        FileUtils.sendFile(fileDTO);
    }


    @Operation(summary = "合并", description = "合并上传的切片文件")
    @PostMapping("merge")
    public ResponseEntity<Void> mergeFile(@Validated @RequestBody MergeFileDTO mergeFileDTO) {

        LoginUser user = SpringWebUtils.requireLogin();
        fileService.mergeFile(mergeFileDTO);

        FileUploadDTO uploadDTO = new FileUploadDTO();
        BeanCopy.from(mergeFileDTO).to(uploadDTO).copy();
        //将合并好的文件保存数据库
        fileService.speedUpload(user.getId(), uploadDTO);
        log.info("用户[{}], 合并[{}]文件", user.getUsername(), uploadDTO.getIdentifier());
        return ResponseEntity.ok().build();
    }
}
