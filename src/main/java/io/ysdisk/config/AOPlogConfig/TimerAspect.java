package io.ysdisk.config.AOPlogConfig;

/**
 * 利用AOP，记录所有DAO层对数据的操作
 */

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;


@Slf4j
@Aspect
@Configuration
public class TimerAspect {

	@Pointcut("execution(* io.ysdisk.dao.*.*(..))")
	public void sqlTimer(){}

	@Around("sqlTimer()")
	public Object daoTimeLog(ProceedingJoinPoint joinPoint) throws Throwable {
		// 记录起始时间
		long begin = System.currentTimeMillis();
		// 执行目标方法
		Object result = joinPoint.proceed();
		// 记录操作时间
		long ms = System.currentTimeMillis() - begin;
		if (ms >= 500L){
			log.warn("{} 执行时间为: {}毫秒", joinPoint.getSignature(), ms);
		}
		return result;
	}
}
