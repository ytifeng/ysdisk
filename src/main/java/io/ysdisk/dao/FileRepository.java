package io.ysdisk.dao;

import io.ysdisk.domain.entity.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;


public interface FileRepository extends JpaRepository<FileEntity, String> {

	List<FileEntity> findAllByCountAndCreateTimeBefore(Long count, Date date);
}
