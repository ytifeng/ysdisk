package io.ysdisk.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件路径树
 * @Author: Richard.Lee
 * @Date: created by 2021/4/5
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "文件基本信息VO",required = true)
public class PathNodeVO {
	/**
	 * 用户文件Id
	 */
	private String id;
	/**
	 * 父id
	 */
	private String pid;
	/**
	 * 用户文件名
	 */
	private String name;

	public PathNodeVO self(){
		return this;
	}
}
