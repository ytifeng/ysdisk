package io.ysdisk.utils;

import io.ysdisk.domain.enums.ThumbnailTypeEnum;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;

import static io.ysdisk.domain.consts.FileConst.*;

/**
 * 根据传递的参数，生成相应的切片，文件，缩略图路径
 */

@Slf4j
public class PathUtils {

	/**
	 * 生成切片文件根目录
	 * @param identifier
	 * @return
	 */
	public static Path getChunkRootPath(String identifier){
		return Path.of(UPLOAD_FOLDER, CHUNK_PATH, identifier);
	}

	/**
	 * 生成文件切片目录
	 * @param identifier
	 * @return
	 */
	public static Path getChunkDirPath(String identifier, Long chunkSize){
		return Path.of(getChunkRootPath(identifier).toString(), String.valueOf(chunkSize));
	}

	/**
	 * 生成切片文件路径
	 * @return
	 */
	public static Path getChunkFilePath(String fileId, Long chunkSize, Integer chunkNumber) {
		Path path = Path.of(getChunkDirPath(fileId, chunkSize).toString(), String.valueOf(chunkNumber));
		return FileUtils.mkdirs(path, false);
	}

	/**
	 * 生成文件路径，文件路径规则：文件上传路径/文件类型/文件唯一标识
	 * @return
	 */
	public static Path getFilePath(String mimetype, String identifier) {

		Path path = Path.of(UPLOAD_FOLDER, mimetype, identifier);
		return FileUtils.mkdirs(path, false);
	}

	/**
	 * 生成略缩图文件路径
	 */
	public static Path getThumbnailFilePath(String identifier, String extension, ThumbnailTypeEnum type){

		String filename = String.format("%s%s.%s", identifier, type.getSuffixName(), extension);
		Path path = Path.of(UPLOAD_FOLDER, THUMBNAIL_PATH, filename);
		return FileUtils.mkdirs(path, false);
	}

}
