package io.ysdisk.config.AOPlogConfig;

import io.ysdisk.domain.entity.UserTokenEntity;
import io.ysdisk.utils.SpringWebUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

/**
 * 针对AOP记录令牌操作
 */
@Slf4j
@Aspect
@Configuration
public class UserTokenAspect {

    @Pointcut("execution(* io.ysdisk.service.impl.UserTokenServiceImpl.addToken(io.ysdisk.domain.entity.UserTokenEntity, String, java.util.Date))")
    public void addTokenCut() {
    }

    @After("addTokenCut()")
    public void addToken(JoinPoint joinPoint) {

        Object[] args = joinPoint.getArgs();
        String email = ((UserTokenEntity) args[0]).getEmail();
        log.info("用户[{}]添加令牌[{}]", Objects.isNull(SpringWebUtils.getRequestUser()) ? email : SpringWebUtils.requireLogin().getUsername(), args[0]);
    }

    @Pointcut("execution(* io.ysdisk.service.impl.UserTokenServiceImpl.useUserToken(String, String, io.ysdisk.domain.enums.TokenTypeEnum))")
    public void userTokenCut() {
    }

    @After("userTokenCut()")
    public void useToken(JoinPoint joinPoint) {

        Object[] args = joinPoint.getArgs();
        log.info("用户[{}]使用类型[{}]的令牌[{}]", Objects.isNull(SpringWebUtils.getRequestUser()) ? args[0] : SpringWebUtils.requireLogin().getUsername(), args[2], args[1]);
    }
}
