package io.ysdisk.controller;

import io.ysdisk.domain.dto.FileDTO;
import io.ysdisk.domain.dto.FileShareDTO;
import io.ysdisk.domain.vo.LoginUser;
import io.ysdisk.service.FileShareService;
import io.ysdisk.utils.FileUtils;
import io.ysdisk.utils.SpringWebUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author: Richard.Lee
 * @Date: created by 2021/4/21
 */
@Tag(name = "FileShare", description = "该接口为用户文件分享接口，主要是做用户文件分享，删除，下载等操作")
@RestController
@Validated
@RequestMapping("/share")
public class ShareController {

	@Autowired
	private FileShareService fileShareService;

	@Operation(summary = "分享文件", description = "前端提供分享文件id和分享条件，后端生成加密的分享链接", tags = {"fileShare"})
	@PostMapping(value = "/file")
	@Parameters({
			@Parameter(name = "userFileId", description = "文件id", required = true),
	})
	public ResponseEntity<FileShareDTO> fileShare(@Validated FileShareDTO fileShareDTO) throws Exception {
		LoginUser user = SpringWebUtils.requireLogin();
		return ResponseEntity.ok(fileShareService.shareFile(fileShareDTO, user.getId()));
	}

	@Operation(summary = "下载文件", description = "文件下载接口，保证文件安全，阻止非法用户下载")
	@GetMapping("/download")
	@Parameters({
			@Parameter(name = "userFileId", description = "用户文件id，需要登录用户的文件", required = true)
	})
	public void anonymousDownloadFile(@RequestParam("shareId") @NotBlank @Length(min = 63, max = 64) String shareId){
		FileDTO fileDTO = fileShareService.getFileResource(shareId);
		FileUtils.sendFile(fileDTO);
	}

	@Operation(summary = "移除分享文件", description = "取消分享", tags = {"fileShare"})
	@PostMapping(value = "/cancel/file")
	public ResponseEntity<Void> cancelShareFile(@Length(min = 32, max = 32) String shareId){
		LoginUser user = SpringWebUtils.requireLogin();
		fileShareService.cancelShareFile(shareId, user.getId());
		return ResponseEntity.ok().build();
	}

	@Operation(summary = "移除分享文件", description = "批量取消分享", tags = {"fileShare"})
	@PostMapping(value = "/batch/cancel/file")
	public ResponseEntity<Void> batchCancelShareFile(@RequestParam(name = "shareIds")@NotEmpty List<String> shareIds){
		LoginUser user = SpringWebUtils.requireLogin();
		fileShareService.batchCancelShareFile(shareIds, user.getId());
		return ResponseEntity.ok().build();
	}

	@Operation(summary = "取消分享文件组", description = "取消分享组", tags = {"fileShare"})
	@PostMapping(value = "/cancel/group")
	public ResponseEntity<Void> cancelShareGroup(@Length(min = 32, max = 32)String shareGroupId){
		LoginUser user = SpringWebUtils.requireLogin();
		fileShareService.cancelShareGroup(shareGroupId, user.getId());
		return ResponseEntity.ok().build();
	}

	@Operation(summary = "取消分享文件组", description = "批量取消分享组", tags = {"fileShare"})
	@PostMapping(value = "/batch/cancel/group")
	public ResponseEntity<Void> batchCancelShareGroup(@RequestParam(name = "shareGroupIds") @NotEmpty List<String> shareGroupIds){
		LoginUser user = SpringWebUtils.requireLogin();
		fileShareService.batchCancelShareGroup(shareGroupIds, user.getId());
		return ResponseEntity.ok().build();
	}


	@Operation(summary = "保存分享文件", description = "保存分享文件", tags = {"fileShare"})
	@PostMapping(value = "/save")
	public ResponseEntity<Void> saveShareFile(@Length(min = 32, max = 32) String shareId, @Length(min = 32, max = 32) String pid, @Length(min = 6, max = 6)String key){
		LoginUser user = SpringWebUtils.requireLogin();
		fileShareService.saveShareFile(List.of(shareId), pid, user.getId(), key);
		return ResponseEntity.ok().build();
	}

	@Operation(summary = "保存分享文件", description = "批量保存分享文件", tags = {"fileShare"})
	@PostMapping(value = "/batch/save")
	public ResponseEntity<Void> batchSaveShareFile(@RequestParam(value = "shareIds") @NotEmpty List<String> shareIds, @Length(min = 32, max = 32) String pid, @Length(min = 6, max = 6) String key){
		LoginUser user = SpringWebUtils.requireLogin();
		fileShareService.saveShareFile(shareIds, pid, user.getId(), key);
		return ResponseEntity.ok().build();
	}
}
