package io.ysdisk.dao;

import io.ysdisk.domain.entity.UserTokenEntity;
import io.ysdisk.domain.enums.TokenTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * 用户令牌DAO
 */
public interface UserTokenRepository extends JpaRepository<UserTokenEntity, Long> {
	// findByEmailAndType 和 findByEmailAndTypeAndToken ，Spring Data JPA会根据方法的命名规则自动生成对应的查询语句，无需你自己实现这些方法的具体逻辑。
	Optional<UserTokenEntity> findByEmailAndType(String email, TokenTypeEnum type);
	Optional<UserTokenEntity> findByEmailAndTypeAndToken(String email, TokenTypeEnum type, String token);
}
