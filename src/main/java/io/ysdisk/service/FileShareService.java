package io.ysdisk.service;

import io.ysdisk.domain.dto.FileDTO;
import io.ysdisk.domain.dto.FileShareDTO;
import io.ysdisk.domain.entity.FileShareEntity;

import java.util.List;


public interface FileShareService {

	/**
	 * 分享文件
	 */
	FileShareDTO shareFile(FileShareDTO fileShareDTO, Long userId) throws Exception;

	/**
	 * 取消分享文件
	 */
	void cancelShareFile(String shareId, Long userId);

	/**
	 * 批量取消分享
	 * @param shareIds
	 * @param userId
	 */
	void batchCancelShareFile(List<String> shareIds, Long userId);

	/**
	 * 取消文件分享组
	 * @param shareGroupId
	 * @param userId
	 */
	void cancelShareGroup(String shareGroupId, Long userId);

	/**
	 * 批量取消文件分享组
	 * @param shareGroupIds
	 * @param userId
	 */
	void batchCancelShareGroup(List<String> shareGroupIds, Long userId);

	/**
	 * 保存分享文件
	 * @param shareIds
	 * @param userId
	 * @param key
	 */
	void saveShareFile(List<String> shareIds, String pid, Long userId, String key);

	/**
	 * 保存次数+1
	 * @return
	 */
	void sCountPlusOne(FileShareEntity fileShareEntity);

	/**
	 * 下载次数+1
	 * @return
	 */
	void dCountPlusOne(FileShareEntity fileShareEntity);

	/**
	 * 获取分享文件下载
	 * @param shareId
	 * @return
	 */
	FileDTO getFileResource(String shareId);


}
