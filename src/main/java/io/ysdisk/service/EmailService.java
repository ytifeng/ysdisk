package io.ysdisk.service;

import javax.mail.MessagingException;

public interface EmailService {
	/**
	 * 发送简单文本的邮件方法
	 * @param to
	 * @param subject
	 * @param content
	 */
	void sendSimpleMail(String to,String subject,String content);

	/**
	 * 发送HTML邮件的方法
	 * @param to
	 * @param subject
	 * @param content
	 */
	void sendHtmlMail(String to ,String subject,String content) throws MessagingException;
}
