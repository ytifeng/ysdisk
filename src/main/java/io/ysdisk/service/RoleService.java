package io.ysdisk.service;

import io.ysdisk.domain.entity.RoleEntity;
import io.ysdisk.domain.enums.RoleEnum;


public interface RoleService {
	/**
	 * 通过RoleEnum查询Role，如不存在该Role则创建
	 * @param roleEnum
	 * @return
	 */
	RoleEntity findByEnum(RoleEnum roleEnum);
}
