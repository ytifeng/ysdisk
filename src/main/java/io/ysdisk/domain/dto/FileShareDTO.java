package io.ysdisk.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.ysdisk.domain.entity.FileShareGroupEntity;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;

import java.util.Date;
import java.util.List;

/**
 * @Author: Richard.Lee
 * @Date: created by 2021/4/21
 */
@Data
@Schema(name = "文件分享DTO", required = true)
public class FileShareDTO {

	/**
	 * {@link io.ysdisk.domain.entity.UserFileEntity} 的 ID
	 */
	@Schema(description = "用户文件Id，可以是文件也可以是文件夹", required = true)
	private List<String> userFileIds;
	@Schema(description = "截至时间，不传为不限时间", nullable = true, required = false)
	private Date dueDate;
	@Schema(description = "6位分享密钥", nullable = true, required = false)
	@Length(min = 6, max = 6)
	private String key;
	@Schema(description = "允许匿名下载", nullable = false, required = true)
	private Boolean anonymousDownload;
	@Schema(description = "分享大厅可见", nullable = false, required = true)
	private Boolean visible;
	@Schema(description = "文件分享url", nullable = false, required = true)
	private String url;
	@Schema(description = "分享文件用户id", nullable = false, required = true)
	public String userFileId;
	@Schema(description = "允许下载次数", nullable = false, required = true)
	private Long dCount;
	public FileShareGroupEntity createFileShareGroupEntity(){
		FileShareGroupEntity groupEntity = new FileShareGroupEntity();
		BeanUtils.copyProperties(this, groupEntity);
		groupEntity.setCreateTime(new Date());
		groupEntity.setActive(true);
		return groupEntity;
	}


}
